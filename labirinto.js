const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];
let linhas = map.length;
let col = map[0].length;
let celulasTotal = linhas*col;
// let count= 0;
let container = document.getElementById("container")
let lines;
function celulas(lines, columns) {
  
  for(let i = 0; i < lines; i++) {
    let lines = document.createElement("div")
    lines.classList.add("lines")
    container.appendChild(lines)
    for(let j = 0; j < columns; j++) {
      let celula = document.createElement("div")
      if(map[i][j] === 'W') {
        celula.classList.add("wall")
        celula.id = `n${i}-${j}`;
      }
      if(map[i][j] === ' ') {
        celula.classList.add("espaço")
        celula.id = `n${i}-${j}`;
      }
      if(map[i][j] === 'S') {
        // celula.id = "start"
        celula.id = `n${i}-${j}`;
      }
      if(map[i][j] === 'F') {
        // celula.id = "end"
        celula.id = `n${i}-${j}`;
        celula.classList.add("final")
      }
      lines.appendChild(celula)  
    }
  }
}

celulas(linhas, col)
// IMAGEM DO FINAL
let final = document.getElementById("n8-20")
let finalImg = document.createElement("i")
finalImg.id = "finalImg"
finalImg.classList.add("far")
finalImg.classList.add("fa-flag")
final.appendChild(finalImg)
// CRIAÇÃO E MOVIMENTO DO JOGADOR
let player = document.createElement("div");
player.id = "player";
    // variável referente ao START
let celPlayer = document.getElementById("n9-0")
celPlayer.appendChild(player)
let imgPlayer = document.createElement("i")
imgPlayer.id = "imgPlayer"
imgPlayer.classList.add("fas")
imgPlayer.classList.add("fa-biking")
player.appendChild(imgPlayer)

// MOVIMENTO DO JOGADOR
  //variável celPlayer é a do start,
  let startLinha = 9;
  // máximo 14 linhas, começando com 0
  let startColuna = 0
  // máximo 20 colunas, começando com 0
  let idAtual;
  let mov;
// VARIÁVEL DA CONDIÇÃO DE VITÓRIA
let endGame = document.getElementById("endGame")
let startMsg = document.getElementById("title")
// FUNÇÃO RELOADPAGE
let btn = document.getElementById("button")
function reloadPage(){
  location.reload();
}
// EVENT LISTENER
document.addEventListener('keydown', (event) => {
  const keyName = event.key;

  if(keyName === "ArrowDown") {
    if(startLinha < 14 && startLinha >= 0) {
      startLinha += 1
      idAtual = `n${startLinha}-${startColuna}`
      mov = document.getElementById(idAtual)
      if(mov.getAttribute("class") === 'wall') {
        startLinha -= 1;
      } else {
        mov.appendChild(imgPlayer)
      }
    }
  }

  if(keyName === "ArrowUp") {
    if(startLinha <= 14 && startLinha > 0) {
      startLinha -= 1
      idAtual = `n${startLinha}-${startColuna}`
      mov = document.getElementById(idAtual)
      if(mov.getAttribute("class") === 'wall') {
        startLinha += 1;
      } else {
        mov.appendChild(imgPlayer)
      }
    }
  }

  if(keyName === "ArrowLeft") {
    if(startColuna > 0 && startColuna <= 20) {
        startColuna -= 1;
        idAtual = `n${startLinha}-${startColuna}`
        mov = document.getElementById(idAtual)
        if(mov.getAttribute("class") === 'wall') {
          startColuna += 1;
        } else {
          mov.appendChild(imgPlayer)
        }
    }
  }

  if(keyName === "ArrowRight") {
    if(startColuna >= 0 && startColuna < 20) {
      startColuna += 1;
      idAtual = `n${startLinha}-${startColuna}`
      mov = document.getElementById(idAtual)
      if(mov.getAttribute("class") === 'wall') {
        startColuna -= 1;
      } else {
        mov.appendChild(imgPlayer)
      }
    }
  }
  if(mov.getAttribute("class") === 'final') {
    endGame.classList.remove("hidden")
    startMsg.classList.add("hidden")
    imgPlayer.classList.add("hidden")
    btn.classList.remove("hidden")
  }
});
// BUTTON
btn.addEventListener("click", reloadPage)
